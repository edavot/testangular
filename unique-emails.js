var inputs = [
    'test.email+alex@kavak.com',
    'test.e.mail+bob.cathy@kavak.com',
    'testemail+david@ka.vak.com'
];

function uniqueEmails(emails) {
    var newArray = [];
    emails.forEach(e => {
        var nombre = e.substring(0, e.indexOf('@')).replace(/\./gi, '');
        nombre = nombre.substring(0, nombre.indexOf('+'));
        var dominio = e.substring(e.indexOf('@'), e.length);
        if (newArray.indexOf(nombre + dominio) === -1) {
            newArray.push(nombre + dominio);
        }
    });
    console.log('uniqueEmails', {  Entrada: emails, Salida: newArray })
    return newArray;
}