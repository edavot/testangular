import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';

const dbConfig: DBConfig  = {
  name: 'TestKavak',
  version: 1,
  objectStoresMeta: [{
    store: 'users',
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: 'correo', keypath: 'correo', options: { unique: true } },
      { name: 'telefono', keypath: 'telefono', options: { unique: false } },
      { name: 'nombre', keypath: 'nombre', options: { unique: false } },
      { name: 'apellido', keypath: 'apellido', options: { unique: false } },
      { name: 'identificacion', keypath: 'identificacion', options: { unique: false } },
      { name: 'contrasenia', keypath: 'contrasenia', options: { unique: false } }
    ]
  }]
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxIndexedDBModule.forRoot(dbConfig)
  ],
  exports: [ NgxIndexedDBModule ]
})

export class AuthModule { }
