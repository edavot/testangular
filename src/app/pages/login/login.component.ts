import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from '../../clases/custom-validators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  private correo: FormControl = new FormControl('', Validators.compose([Validators.required, CustomValidators.ValidarCorreo]));
  private contrasenia: FormControl = new FormControl('', Validators.compose([Validators.required]));

  public loginForm: FormGroup = new FormGroup({
    correo: this.correo,
    contrasenia: this.contrasenia
  });

  public activarRegistro = false;

  constructor(private loginServive: LoginService, private router: Router) { }

  ngOnInit() { }

  public onSubmit() {
    this.loginForm.disable();
    const user = { correo: this.correo.value, contrasenia: this.contrasenia.value };
    this.loginServive.login(user)
      .then(() => {
        this.router.navigate(['/posts']);
      })
      .catch((err) => {
        console.log(err);
        this.activarRegistro = true;
       })
      .finally(() =>  this.loginForm.enable());
  }

}
