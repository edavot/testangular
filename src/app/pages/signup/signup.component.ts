import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { CustomValidators } from '../../clases/custom-validators';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {

  private readonly namePattern = '^[a-zA-Z]+$';
  private readonly identificacioPattern = '^[1-9]+$';

  private correo: FormControl =
    new FormControl('', Validators.compose([Validators.required, CustomValidators.ValidarCorreo]));
  private telefono: FormControl =
    new FormControl('', Validators.compose([Validators.required, CustomValidators.ValidarTelefono]));
  private nombre: FormControl =
    new FormControl('', Validators.compose([Validators.required, Validators.pattern(this.namePattern)]));
  private apellido: FormControl =
    new FormControl('', Validators.compose([Validators.required, Validators.pattern(this.namePattern)]));
  private identificacion: FormControl =
    new FormControl('', Validators.compose([Validators.required, Validators.pattern(this.identificacioPattern)]));
  private contrasenia: FormControl =
    new FormControl('', Validators.compose([Validators.required]));

  public signupForm: FormGroup = new FormGroup({
    correo: this.correo,
    telefono: this.telefono,
    nombre: this.nombre,
    apellido: this.apellido,
    identificacion: this.identificacion,
    contrasenia: this.contrasenia
  });

  public response = { error: false, message: '' };

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() { }

  onSubmit() {
    this.signupForm.disable();
    const user = {
      correo: this.correo.value,
      telefono: this.telefono.value,
      nombre: this.nombre.value,
      apellido: this.apellido.value,
      identificacion: this.identificacion.value,
      contrasenia: this.contrasenia.value
    };
    this.loginService.signup(user)
    .then(resp => {
      if (resp) {
        this.router.navigate(['/posts']);
      } else {
        this.response = {
          error: true,
          message: ''
        }
      }
    })
    .catch((err) => {
      console.error(err);
    })
    .finally(() => this.signupForm.enable());
  }

}
