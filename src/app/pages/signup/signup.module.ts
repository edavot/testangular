import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup.component';
import { SignupRoutingModule } from './sigup-routing.modules';
import { AuthModule } from '../../guards/auth.module';

@NgModule({
  declarations: [
    SignupComponent
  ],
  imports: [
    CommonModule,
    SignupRoutingModule,
    AuthModule,
    ReactiveFormsModule
  ]
})

export class SignupModule { }
