import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPost } from './post.model';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html'
})
export class PostsComponent implements OnInit {
  public filtering = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() { }

  public getData(): Observable<IPost[]> {
    return this.route.data.pipe(
      map((data: { posts: IPost[] }) => {
        return data.posts.filter((item) => {
          return item.title.toUpperCase().indexOf(this.filtering.toUpperCase()) !== -1 ||
                 item.body.toUpperCase().indexOf(this.filtering.toUpperCase()) !== -1 ;
        });
      })
    );
  }

}
