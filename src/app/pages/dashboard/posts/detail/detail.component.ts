import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPost } from '../post.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit, OnDestroy {
  public post: IPost;
  private postSubscription: Subscription = new Subscription();
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.postSubscription = this.route.data.subscribe((data: { post: IPost }) => {
      this.post = data.post;
    });
  }

  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }
}