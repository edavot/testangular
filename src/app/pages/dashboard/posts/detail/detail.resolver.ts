import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { PostsService } from '../../../../services/posts.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class DetailResolver implements Resolve<any> {

  constructor(private postsService: PostsService) { }

  resolve(route: ActivatedRouteSnapshot) {
    const id = Number(route.paramMap.get('id'));
    return this.postsService.getById(id).pipe(
      catchError( err => {
        return of({});
      })
    );
  }
}
