import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { PostsService } from '../../../services/posts.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()

export class PostsResolver implements Resolve<any> {

  constructor(private postsService: PostsService) { }

  resolve() {
    return this.postsService.get().pipe(
      catchError( err => {
        return of([]);
      })
    );
  }
}
