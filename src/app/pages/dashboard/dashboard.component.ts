import { Component, OnDestroy } from '@angular/core';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements  OnDestroy {

  constructor(private loginService: LoginService) { }

  ngOnDestroy() {
    this.loginService.logOut();
  }

}
