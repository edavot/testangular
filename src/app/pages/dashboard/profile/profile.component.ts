import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IUser, LoginService } from '../../../services/login.service';
import { CustomValidators } from '../../../clases/custom-validators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {


  private user: IUser = this.loginService.getUserAuth();
  private readonly namePattern = '^[a-zA-Z]+$';
  private readonly identificacioPattern = '^[1-9]+$';

  private correo: FormControl =
    new FormControl(this.user.correo, Validators.compose([Validators.required, CustomValidators.ValidarCorreo]));
  private telefono: FormControl =
    new FormControl(this.user.telefono, Validators.compose([Validators.required, CustomValidators.ValidarTelefono]));
  private nombre: FormControl =
    new FormControl(this.user.nombre, Validators.compose([Validators.required, Validators.pattern(this.namePattern)]));
  private apellido: FormControl =
    new FormControl(this.user.apellido, Validators.compose([Validators.required, Validators.pattern(this.namePattern)]));
  private identificacion: FormControl =
    new FormControl(this.user.identificacion, Validators.compose([Validators.required, Validators.pattern(this.identificacioPattern)]));
  private contrasenia: FormControl =
    new FormControl(this.user.contrasenia, Validators.compose([Validators.required]));

  public profileForm: FormGroup = new FormGroup({
    correo: this.correo,
    telefono: this.telefono,
    nombre: this.nombre,
    apellido: this.apellido,
    identificacion: this.identificacion,
    contrasenia: this.contrasenia
  });

  public success = false;
  public error = false;

  constructor(private loginService: LoginService) { }

  ngOnInit() { }

  onSubmit() {
    this.profileForm.disable();
    const user = {
      id: this.user.id,
      correo: this.correo.value,
      telefono: this.telefono.value,
      nombre: this.nombre.value,
      apellido: this.apellido.value,
      identificacion: this.identificacion.value,
      contrasenia: this.contrasenia.value
    };
    this.loginService.updateProfile(user)
    .then(resp => this.success = true)
    .catch(err => this.error = true)
    .finally(() => this.profileForm.enable());
  }

}
