import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { PostsComponent } from './posts/posts.component';
import { DetailComponent } from './posts/detail/detail.component';

import { PostsResolver } from './posts/posts.resolver';
import { DetailResolver } from './posts/detail/detail.resolver';
import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [ AuthGuard ],
    children: [
      { path: '', redirectTo: 'posts' },
      { path: 'profile', component: ProfileComponent },
      { path: 'posts', component: PostsComponent, resolve: { posts: PostsResolver } },
      { path: 'detail/:id', component: DetailComponent, resolve: { post: DetailResolver } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ PostsResolver, DetailResolver ]
})
export class DashboardRoutingModule { }
