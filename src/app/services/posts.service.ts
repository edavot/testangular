import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { IPost } from '../pages/dashboard/posts/post.model';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  public get(): Observable<any> {
    const URL_REQUEST = `${environment.apiRest}/posts`;
    return this.http.get(URL_REQUEST).pipe(
      map((data: IPost[]) => {
        for (const item of data) {
          item.title = this.Capitalize(item.title);
        }
        return data;
      }));
  }

  public getById(id: number): Observable<any> {
    const URL_REQUEST = `${environment.apiRest}/posts/${id}`;
    return this.http.get(URL_REQUEST).pipe(
      map((data: IPost) => {
        return { ...data, title: this.Capitalize(data.title)  };;
      }));
  }

  private Capitalize(value: string): string {
    return value.replace(/\w\S*/g, (text) => {
      return `${text.charAt(0).toUpperCase()}${text.substr(1).toLowerCase()}`;
    });
  }

}
