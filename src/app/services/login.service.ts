import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private userIsAuth = false;
  private userAuth: IUser | null;

  constructor(private dbService: NgxIndexedDBService) { }

  public signup(user: IUser): Promise<any> {
    return this.dbService.add('users', user)
    .then(value => {
      return new Promise((resolve, reject) => {
        if (value) {
          this.userIsAuth = true;
          this.userAuth = user;
          reject('Ocurrio un error al registrar los datos');
          // resolve(true);
        } else {
          reject('Ocurrio un error al registrar los datos');
        }
      });
    });
  }

  public login(user: IUser): Promise<any> {
    return this.dbService.getByIndex('users', 'correo', user.correo)
    .then((value: IUser) => {
        return new Promise((resolve, reject) => {
          if (value && user.contrasenia ===  value.contrasenia) {
            this.userIsAuth = true;
            this.userAuth = value;
            resolve(value);
          } else {
            reject('Usuario o contraseña incorrectos');
          }
        });
    });
  }

  public updateProfile(user: IUser): Promise<any> {
    return this.dbService.update('users',  user)
    .then((value: IUser) => {
      return new Promise((resolve, reject) => {
        if (value) {
          this.userAuth = user;
          resolve(true);
        } else {
          reject('Ocurrio un error al actualizar los datos');
        }
      });
    });
  }

  public isAuth() { return this.userIsAuth; }

  public getUserAuth() { return this.userAuth; }

  public logOut() {
    this.userAuth = null;
    this.userIsAuth = false;
  }
}

export interface IUser {
  id?: number;
  correo: string;
  telefono?: string;
  nombre?: string;
  apellido?: string;
  identificacion?: number;
  contrasenia: string;
}
