import {  AbstractControl } from '@angular/forms';
export class CustomValidators {

    public static ValidarCorreo(control: AbstractControl): { [key: string]: boolean } | null  {
        const emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        return !emailRegex.test(control.value) ? { validarCorreo: true } : null;
    }

    public static ValidarTelefono(control: AbstractControl): { [key: string]: boolean } | null  {
        const phoneRegex = /([0-9][ -]*){10}$/;
        return !phoneRegex.test(control.value) ? { validarTelefono: true } : null;
    }
}
